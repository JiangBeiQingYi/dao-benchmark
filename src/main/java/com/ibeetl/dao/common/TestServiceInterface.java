package com.ibeetl.dao.common;

public interface TestServiceInterface {
	public void testAdd();
	public void testUnique();
	public void testUpdateById();
	public void testPageQuery();
	public void testExampleQuery();
}
