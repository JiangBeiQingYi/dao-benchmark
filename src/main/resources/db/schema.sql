CREATE TABLE sys_user (
   ID int(20) NOT NULL ,
   CODE  varchar(16) DEFAULT NULL,
   NAME  varchar(16) DEFAULT NULL,
   PASSWORD  varchar(64) DEFAULT NULL,
   CREATE_TIME  datetime(6) DEFAULT NULL,
   ORG_ID  int(65) DEFAULT NULL,
   STATE  varchar(16) DEFAULT NULL,
   JOB_TYPE1  varchar(16) DEFAULT NULL,
   DEL_FLAG  tinyint(6) DEFAULT NULL,
   update_Time  datetime DEFAULT NULL,
   JOB_TYPE0  varchar(16) DEFAULT NULL,
   PRIMARY KEY ( ID )
) ;